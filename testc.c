#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// in one file
void readfile(char **filename, double *vector)
{
	
	FILE *file = fopen(*filename, "r");
	size_t  len = 256;                    // number of bytes initially allocated for each line
	char *buffer = malloc(len * sizeof(char)); //the variable for the one line of character
	int n = 0, i;                         //n: index of current line number
	char *total, *toll; 
	getline(&buffer, &len, file);         //skip the first line since it contains header
	while( -1!= getline(&buffer, &len, file)){    //return -1 when failure to read a line or end of file 
		char *buffer2 = buffer;               //define a pointer points to the same address with pointer
		for(i = 0; i < 10; i++){
			toll = strsep(&buffer2, ",");          //get the 10th field, the previous are separated by ,
		}
            total = strsep(&buffer2, "\n");        //get the 11th field, this is separated by /n
     vector[n]= atof(total) - atof(toll);          //convert string argument to floating-point number, total_amount-toll_amount
     n = n+1;	 
	}
	fclose(file);
	free(buffer);
}

