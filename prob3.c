#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// in one file 
void getValue2(char **filefare, char** filedata, double *sum_x1, double *sum_x2, double *sum_y, double *sum_x1y, double *sum_x2y,
               double *sum_x1x2, double *sum_x1_2, double *sum_x2_2, double *strlen)
{
	//strlen: the number of used lines for later regression, delete non-positive values 
	//other statistics are initialized as 0
	FILE *file_fare = fopen(*filefare, "r");
	FILE *file_data = fopen(*filedata, "r");
	size_t  len1 = 256, len2 = 256;                    // number of bytes initially allocated for each line
	char *buffer_fare = malloc(len1 * sizeof(char));    //allocate space for one line of character
	char *buffer_data = malloc(len2 * sizeof(char));
	int i;                         //n: index of current line number
	char *total, *toll, *timesec, *surcharge; 
	getline(&buffer_fare, &len1, file_fare);         //skip the first line since it contains header
	getline(&buffer_data, &len2, file_data);
	//munipulate on single line
	while( -1!= getline(&buffer_fare, &len1, file_fare) && -1!=getline(&buffer_data, &len2, file_data)){    //end of file, return -1
		char *buffer2_fare = buffer_fare;               //define pointer(buffer) which points to the same address with pointer(buffer)
		char *buffer2_data = buffer_data;
		for(i = 0; i < 7; i++){
			surcharge = strsep(&buffer2_fare, ",");  
            timesec = strsep(&buffer2_data, ",");			
		}  
			timesec = strsep(&buffer2_data, ",");     //distract variable time
            timesec = strsep(&buffer2_data, ",");           	
		    toll = strsep(&buffer2_fare, ","); 
            toll = strsep(&buffer2_fare, ","); 
            toll = strsep(&buffer2_fare, ",");       //get variable toll
            total = strsep(&buffer2_fare, "\n");     //get variable total
			double x1, x2, y;
            y = atof(total) - atof(toll);            //convert string argument to number, total_amount-toll_amount
			x1= atof(surcharge);                      //surchage can take zero numbers
			x2 = atof(timesec);
			if(x1>=0 && x2>0 && y>0)                  //both time and fare should be positive values
			{
				*sum_x1 = *sum_x1+x1;
				*sum_x2 = *sum_x2+x2;
				*sum_y = *sum_y+y;
				*sum_x1y = *sum_x1y+x1*y;
				*sum_x2y = *sum_x2y+x2*y;
				*sum_x1x2 = *sum_x1x2+x1*x2;
				*sum_x1_2 = *sum_x1_2+x1*x1;
				*sum_x2_2 = *sum_x2_2+x2*x2;
				*strlen = *strlen+1;	
			}
            
			
	}
	fclose(file_fare); fclose(file_data);
	free(buffer_fare); free(buffer_data);
}