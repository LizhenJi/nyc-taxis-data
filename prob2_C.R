#problem2 implemented in C

f= list.files("/home/data/NYCTaxis", pattern = "fare.*\\.csv$", full.names = TRUE)
f_data = list.files("/home/data/NYCTaxis", pattern = "data.*\\.csv$", full.names = TRUE)

library(parallel)
cl = makeCluster(12, "FORK")


C_stat = function(num){
  dyn.load("prob2.so")
  filefare = f[num]
  filedata = f_data[num]
  sum_x = 0
  sum_y = 0
  sum_xy = 0
  sum_x2 = 0
  strlen = 0
  .C("getValue",filefare = as.character(filefare), filedata = as.character(filedata), sum_x = as.double(sum_x), 
     sum_y = as.double(sum_y), sum_xy = as.double(sum_xy), sum_x2 = as.double(sum_x2), strlen = as.double(strlen))
  
}

C_statistic_time = system.time({C_statistics = clusterApply(cl, 1:12, C_stat)})

# replicate this procedure for 5 times
# prob2_c_reptime = system.time({prob2_c = replicate(5, clusterApply(cl, 1:12, C_stat), simplify = FALSE )})

#calculate the final statistic with the same method in R
cal_c = function(stats){
  sumx = sum(unlist(lapply(stats, '[', 3)))
  sumy = sum(unlist(lapply(stats, '[', 4)))
  sumxy = sum(unlist(lapply(stats, '[',5)))
  sumx2 = sum(unlist(lapply(stats, '[', 6)))
  n_tot = sum(unlist(lapply(stats, '[', 7)))
  x_bar = sumx/n_tot
  y_bar = sumy/n_tot
  beta_1 = (sumxy-n_tot*x_bar*y_bar)/(sumx2-n_tot*x_bar^2)
  beta_0 = y_bar - beta_1*x_bar
  return(c(beta_1, beta_0))
}
res  = cal_c(C_statistics)
res

