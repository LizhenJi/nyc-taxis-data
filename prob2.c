#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// in one file
void getValue(char **filefare, char** filedata, double *sum_x, double *sum_y, double *sum_xy, double *sum_x2, double *strlen)
{
	//statistics are initialized as 0, strlen is used to store number of used observations
	FILE *file_fare = fopen(*filefare, "r");
	FILE *file_data = fopen(*filedata, "r");
	size_t  len1 = 256, len2 = 256;                    // number of bytes initially allocated for each line
	char *buffer_fare = malloc(len1 * sizeof(char));    //allocate space for one line of character
	char *buffer_data = malloc(len2 * sizeof(char));
	int i;                         
	char *total, *toll, *timesec; 
	getline(&buffer_fare, &len1, file_fare);         //skip the first line since it contains header
	getline(&buffer_data, &len2, file_data);
	while( -1!= getline(&buffer_fare, &len1, file_fare) && -1!=getline(&buffer_data, &len2, file_data)){    //end of file, return -1
		char *buffer2_fare = buffer_fare;               //define pointer(buffer) which points to the same address with pointer(buffer)
		char *buffer2_data = buffer_data;
		for(i = 0; i < 9; i++){
			timesec = strsep(&buffer2_data, ",");          //get first 9 field, separated by ,
			toll = strsep(&buffer2_fare, ",");          
		}
		    toll = strsep(&buffer2_fare, ",");             //get 10th field
            total = strsep(&buffer2_fare, "\n");            //get the 11th field, this is separated by /n
			double x, y;
            y= atof(total) - atof(toll);          //convert string argument to number, total_amount-toll_amount
			x = atof(timesec);
			if(x>0 && y>0 )                     //both time and fare should be positive values
			{
				*sum_x = *sum_x+x;
				*sum_y = *sum_y+y;
				*sum_xy = *sum_xy+x*y;
				*sum_x2 = *sum_x2+x*x;
				*strlen = *strlen+1;	
			}
            
			
	}
	fclose(file_fare); fclose(file_data);
	free(buffer_fare); free(buffer_data);
}