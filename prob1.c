#include <stdio.h>
#include <stdlib.h>
//reference: http://stackoverflow.com/questions/8448790/sort-arrays-of-double-in-c
int cmp(const void *x, const void *y)
{
  double xx = *(double*)x, yy = *(double*)y;
  if (xx < yy) return -1;
  if (xx > yy) return  1;
  return 0;
}

  void sort_C(double* C_table, double* len)
{
   qsort(C_table, *len, sizeof(C_table[0]), cmp);
}
