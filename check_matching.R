
#checking matching of observations in two files

#extract a random sample in each file to check
#filename are corresponding to each other
#using parallel method
f= list.files("/home/data/NYCTaxis", pattern = "fare.*\\.csv$", full.names = TRUE)
f_data = list.files("/home/data/NYCTaxis", pattern = "data.*\\.csv$", full.names = TRUE)

library(parallel)
cl = makeCluster(12, "FORK")
els =  clusterSplit(cl, f)

#index of order of file and data corresponds to each other
#get names of fare and data file
mymatch = function(num){
file = f[num] 
tot = system(sprintf("wc -l %s", file), intern = TRUE)
N= as.numeric(strsplit(tot[length(tot)], " +")[[1]][1])
n = 100 #choose 100 obs out of one file
#using Duncan's sampling code
set.seed(num)
id = sample(N, n)
jumps = diff(c(0, sort(id))) 
con1 = pipe(sprintf("cut -f 1,2,4 -d , %s", file))
con2 = pipe(sprintf("cut -f 1,2,6 -d , %s", f_data[num]))
fare = character(n)
dat = character(n)
for(i in seq_along(jumps))
{
  fare[i] = readLines(con1, jumps[i])[jumps[i]]
  dat[i] = readLines(con2, jumps[i])[jumps[i]]
}
#logical vector indicator of whether this file match
ans = all(fare==dat)
}

match_time = system.time({ans = clusterApply(cl, 1:12, mymatch)})
 


